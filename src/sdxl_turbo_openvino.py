from optimum.intel.openvino.modeling_diffusion import OVStableDiffusionXLPipeline

pipeline = OVStableDiffusionXLPipeline.from_pretrained(
    "rupeshs/sdxl-turbo-openvino-int8",
    cache_dir='../models',
    ov_config={
        "CACHE_DIR": "../models",
    },
)
prompt = "a cat wearing santa claus dress,portrait"

image = pipeline(
    prompt=prompt,
    width=512,
    height=512,
    num_inference_steps=1,
    guidance_scale=1.0,
).images[0]
image.save('../data/sample.png')