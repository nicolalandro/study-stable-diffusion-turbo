import streamlit as st
from optimum.intel.openvino.modeling_diffusion import OVStableDiffusionXLPipeline

#####################################################
#
# Static Variables
#
#####################################################

if not hasattr(st, 'pipeline'):
    st.pipeline = OVStableDiffusionXLPipeline.from_pretrained(
        "rupeshs/sdxl-turbo-openvino-int8",
        cache_dir='../models',
        ov_config={
            "CACHE_DIR": "../models",
        },
    )

#####################################################
#
# Session Variables
#
#####################################################

if "messages" not in st.session_state:
    st.session_state.messages = []

#####################################################
#
# Streamlit APP
#
#####################################################

title = "LLM chat"
st.set_page_config(page_title=title, page_icon=":robot:")
st.header(title)


for message in st.session_state.messages:
    with st.chat_message(message["role"]):
        st.markdown(message["content"])
        if "img" in message.keys():
            st.image(message["img"])

if prompt := st.chat_input("Ask to mistral"):
    # Display user message in chat message container
    with st.chat_message("user"):
        st.markdown(prompt)
    # Add user message to chat history
    st.session_state.messages.append({"role": "user", "content": prompt})

    with st.chat_message("assistant"):
        with st.spinner("Generating..."):
            text = "Generated Image:"
            image = st.pipeline(
                prompt=prompt,
                width=512,
                height=512,
                num_inference_steps=1,
                guidance_scale=1.0,
            ).images[0]
        st.markdown(text)
        st.image(image)
    # Add assistant response to chat history
    st.session_state.messages.append(
        {"role": "assistant", "content": text, "img": image})
