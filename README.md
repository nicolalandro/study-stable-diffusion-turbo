# Study Stable Diffusion Turbo

# Develop

```
pytthon3.9 -m venv venv
source venv/bin/activate.fish
pip install -r requirements.txt

cd src
CUDA_VISIBLE_DEVICES="1" python src/sdxl_turbo.py
# or
python src/sdxl_turbo_openvino.py
# results on data/sample.png

streamlit run streamlit_sdxl_turbo_openvino.py
```

# References
* [Huggingface stablediffusion turbo](https://huggingface.co/stabilityai/sdxl-turbo)
* [Stable diffusion with openvino](https://huggingface.co/rupeshs/sd-turbo-openvino)

